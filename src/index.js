import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import promise from 'redux-promise';
import ContactList from './components/contact_list';
import RegisterForm from './components/register_form';
import Contact from './components/contact';
import rootReducer from './reducers';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(rootReducer)}>
        <BrowserRouter>
            <Switch>
                <Route path="/contact/newcontact" component={RegisterForm} />
                <Route path="/contact/:id" component={Contact} />
                <Route path="/" component={ContactList} />
            </Switch>
        </BrowserRouter>
    </Provider>
, document.querySelector('.container'));