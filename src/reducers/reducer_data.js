import _ from 'lodash';
import { FETCH_CONTACTS, FETCH_ONE_CONTACT, DELETE_CONTACT, UPDATE_CONTACT } from '../actions';


const initialState = {};

export default function(state = initialState, action) {
    switch(action.type) {
        case FETCH_CONTACTS:
            return _.mapKeys(action.payload.data, 'id');
            //return  [ ...action.payload.data, ...state ];
            //return [].concat(action.payload.data).concat(state);
        case FETCH_ONE_CONTACT:
           return { ...state, [action.payload.data.id]: action.payload.data };
        case DELETE_CONTACT:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}