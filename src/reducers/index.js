import { combineReducers } from 'redux';
import DataReducer  from './reducer_data';
import { reducer as formReducer} from 'redux-form';

/**
 * combineReducer map the reducers
 */
const rootReducer = combineReducers({
  contacts: DataReducer,
  form: formReducer
});

export default rootReducer;
