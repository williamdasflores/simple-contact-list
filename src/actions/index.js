import axios from 'axios';

export const FETCH_CONTACTS = 'fetch_contact';
export const CREATE_CONTACT = 'create_contact';
export const FETCH_ONE_CONTACT = 'fetch_one_contact';
export const DELETE_CONTACT = 'delete_contact';
export const UPDATE_CONTACT = 'update_contact';

const ROOT_URL = 'http://localhost:3000/contact';

export function fetchContact() {
    const request = axios.get(`${ROOT_URL}`);

    return {
        type: FETCH_CONTACTS,
        payload: request
    };
}

export function createContact(values, callback) {
    const request = axios.post(`${ROOT_URL}`, values).then(() => callback());

    return {
        type: CREATE_CONTACT,
        payload: request
    }
}

export function fetch_one_contact(id) {
    const request = axios.get(`${ROOT_URL}/${id}`);

    return {
        type: FETCH_ONE_CONTACT,
        payload: request
    }
}

export function delete_contact(id, callback) {
    const request = axios.delete(`${ROOT_URL}/${id}`).then(() => callback());

    return {
        type: DELETE_CONTACT,
        payload: id
    }
}

export function update_contact(id, values, callback) {
    const request = axios.patch(`${ROOT_URL}/${id}`, values)
        .then(() => callback())
        .catch((error) => {
            console.log("ERROR", error);
        });

    return {
        type: UPDATE_CONTACT,
        payload: id
    }
}



