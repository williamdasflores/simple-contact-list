import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createContact } from '../actions'

class RegisterForm extends Component {

    renderInput(field) {
        const { meta: {touched, error} } = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`

        return (
            <div className={className}>
                <input type="text" 
                    {...field.input} 
                    className='form-control'
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    } 

    onSubmit(values) {
        this.props.createContact(values, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const { handleSubmit } = this.props;

       return(
           <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
                <div className="form-row">
                    <div className="form-group col-md-6">   
                        <label>First Name</label>                        
                        <Field name="name" component={this.renderInput} />
                    </div>
                    <div className="form-group col-md-6">
                        <label>Last Name</label>                
                        <Field name="lastName" component={this.renderInput} />
                    </div>
                </div>  
                <div className="form-group col-md-12">
                    <label for="email">Email</label>
                    <Field name="email" component="input" type="email" 
                        className="form-control"/>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label>Telephone</label>                
                        <Field name="telephone" component={this.renderInput} />
                    </div>
                    <div className="form-group col-md-6">
                        <label>Cellphone</label>            
                        <Field name="cellphone" component={this.renderInput} />
                    </div>
                </div>
                <div className="form-group col-md-12">
                    <label for="notes">Notes</label>
                    <Field name="notes" component="textarea" className="form-control"/>
                </div>
                <div className="form-group col-md-4">
                    <button type="submit" className="btn btn-primary">Save</button>
                    <Link to="/" className="btn btn-danger">Cancel</Link>
                </div>
            </form>
       );
    }

}

function validate(values) {
    const  errors = {};

    if (!values.name) {
        errors.name = "Please insert the first name."
    }

    if (!values.lastName) {
        errors.lastName = "Please insert the last name."
    }

    if (!values.telephone) {
        errors.telephone = "Please fill the telephone field."
    }

    return errors;
}

export default reduxForm({
    validate,
    form: 'RegisterForm'
})(
    connect(null, { createContact })(RegisterForm)
);