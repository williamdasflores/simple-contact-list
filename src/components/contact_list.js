import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchContact } from '../actions';
import { Link } from 'react-router-dom';

class ContactList extends Component {
    componentDidMount() {
        this.props.fetchContact();
    }

    renderContact() {
        return _.map(this.props.contacts, contact => {
            return (
                <li className="list-group-item" key={contact.id}>
                    <Link to={`/contact/${contact.id}`} >
                        {contact.name}
                    </Link>
                </li>
            );
        });
    }
    
    render() {
        return (
            <div>
                <div>
                    <Link to="/contact/newcontact" className="btn btn-primary btnAddContact">Add Contact</Link>
                </div>
                <ul className="list-group">
                    {this.renderContact()}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = ({contacts}) => {
    return { contacts };
}

export default connect(mapStateToProps, { fetchContact })(ContactList);