import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { fetch_one_contact, delete_contact, update_contact } from '../actions';
import { connect } from 'react-redux'; 
import { Link } from 'react-router-dom';

class Contact extends Component {
    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.fetch_one_contact(id);
        this.handleInitialize()
    }

    handleInitialize() {
        const { contact } = this.props;
        const initData = {
            "name" : contact.name,
            "lastName" : contact.lastName,
            "email" : contact.email,
            "telephone" : contact.telephone,
            "cellphone" : contact.cellphone,
            "notes" : contact.notes
        };

        this.props.initialize(initData);
    }

    onDeleteClick() {
        const { id } = this.props.match.params;
        this.props.delete_contact(id, () => {
            this.props.history.push("/");
        });
    }

    onSubmit(values) {
        const { id } = this.props.match.params;
        this.props.update_contact(id, values, () => {
            this.props.history.push("/");
        })
    }

    renderInput(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        return (
            <div className={className}>
                <input type="text" 
                    {...field.input}
                    className='form-control'
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    render() {
        const { contact, handleSubmit } = this.props;

        if (!contact) {
            return <div>Loading</div>;
        }
        return (
            <div>
                <Link to="/">Back to Index</Link>
                <div>
                    <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) } >
                        <div className="form-row">
                            <div className="form-group col-md-6">   
                                <label>First Name</label>                        
                                <Field name="name" component={this.renderInput} />
                            </div>
                            <div className="form-group col-md-6">
                                <label>Last Name</label>                
                                <Field name="lastName" component={this.renderInput} />
                            </div>
                        </div>  
                        <div className="form-group col-md-12">
                            <label for="email">Email</label>
                            <Field name="email" component="input" type="email" 
                                className="form-control"/>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Telephone</label>                
                                <Field name="telephone" component={this.renderInput} />
                            </div>
                            <div className="form-group col-md-6">
                                <label>Cellphone</label>            
                                <Field name="cellphone" component={this.renderInput} />
                            </div>
                        </div>
                        <div className="form-group col-md-12">
                            <label for="notes">Notes</label>
                            <Field name="notes" component="textarea" className="form-control"/>
                        </div>
                        <div className="form-group col-md-4">
                            <button type="submit" className="btn btn-primary">Update</button>
                            <button type="button" onClick={this.onDeleteClick.bind(this)} 
                                className="btn btn-danger pull-xs-right">
                            Delete
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        )
    }
}

function validate(values) {
    const  errors = {};

    if (!values.name) {
        errors.name = "Please insert the first name."
    }

    if (!values.lastName) {
        errors.lastName = "Please insert the last name."
    }

    if (!values.telephone) {
        errors.telephone = "Please fill the telephone field."
    }

    return errors;
}

function mapStateToProps({ contacts }, ownProps) {
    return { contact: contacts[ownProps.match.params.id] };
}

export default reduxForm({
    validate,
    form: 'Contact',
})(
    connect(mapStateToProps, { fetch_one_contact, delete_contact, update_contact })(Contact)
);
