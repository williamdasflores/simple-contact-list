import React, { Component } from 'react';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = { search: ''};

        this.onInputChange = this.onInputChange.bind(this);
        this.addContactToList = this.addContactToList.bind(this);
    }

    onInputChange(event) {
        this.setState({
            search: event.target.value
        });
    }

    addContactToList() {
      this.props.onAddContactName(this.state.search);
       this.setState({
        search: ''
       }) 
    }

    render() {
        return (
            <div className="input-group">
                <input
                    className="form-control"
                    value={this.state.search}
                    onChange={this.onInputChange} />
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary"
                        onClick={this.addContactToList}>Submit</button>
                </span>
            </div>
        );
    }
}

export default SearchBar;