import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import SearchBar from './components/search_bar';
import ContactList from './components/contact_list';

class App extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      contactNames : []
    };
    
    this.addContact = this.addContact.bind(this);
  }

  addContact(name) {
    this.setState(prevState => ({
      contactNames: prevState.contactNames.concat(name)
    }));
  }

  handleDelete(itemToDelete) {
      const newItems = this.state.contactNames.filter((_item) => {
        return _item != itemToDelete;
      });

      this.setState({
        contactNames: newItems
      });
  }

  
  render() {
    return (
      <div>
        <SearchBar onAddContactName= { (addContactName) => {this.addContact(addContactName)} } />
        <ContactList showContacts={ this.state.contactNames } handleDelete={this.handleDelete.bind(this)} />
      </div>
    );
  }
}



ReactDOM.render(<App /> , document.querySelector('.container'));
